/* global module */
'use strict'

const crypto = require('crypto');

class Pherr {

  /**
   * Create a new Pherr Error instance.
   * @param {number|string} code An application understood error code.
   * @param {...string} messages Any number of messages to describe the Error
   * @returns {Error|*} A specially configured Error instance
   */
  constructor(code, ...messages) {
    const rawError = new Error()

    if (code) {
      rawError.code = code
    }
    rawError.message = messages.join('\n')
    rawError.id = crypto.randomBytes(20).toString('hex')

    rawError.push = this.push.bind(this)

    this._rawError = rawError
    return this._rawError
  }

  /**
   * Adds a new message to the existing Pherr Error instance
   * @param {string} message A new message to add to any existing messages.
   * @returns {Error|*}
   */
  push(message) {
    if (typeof this._rawError.message !== 'string') {
      this._rawError.message = ''
    }

    this._rawError.message += `\n${message}`
    return this._rawError
  }
}

module.exports = Pherr
