/* global after, before, describe, it */
'use strict'

const expect = require('chai').expect

const Pherr = require('../src/Pherr')

describe('Utility: Pherr', () => {
  const testCode = 'TEST_CODE'

  describe('instantiation', () => {

    it('returns an instance of Error when called', () => {
      const err = new Pherr()
      expect(err).to.be.a('Error')
      expect(err instanceof Error).to.equal(true)
    })

    it('sets a code when provided as a string', () => {
      const err = new Pherr('TEST_CODE')
      expect(err.code).to.be.a('string')
      expect(err.code).to.equal('TEST_CODE')
    })

    it('sets a code when provided as a number', () => {
      const err = new Pherr(42)
      expect(err.code).to.be.a('number')
      expect(err.code).to.equal(42)
    })

    it('sets a random id', () => {
      const err = new Pherr()
      expect(err.id).to.be.a('string')
    })

    it('does not set a message when no message is provided', () => {
      const err = new Pherr(testCode)
      expect(err.message).to.be.a('string')
      expect(err.message).to.equal('')
    })

    it('sets a message when provided', () => {
      const err = new Pherr(testCode, 'message')
      expect(err.message).to.be.a('string')
      expect(err.message).to.equal('message')
    })

    it('accepts multiple messages and parses them with line breaks', () => {
      const err = new Pherr(testCode, 'message1', 'message2')
      expect(err.message).to.be.a('string')
      expect(err.message).to.equal('message1\nmessage2')
    })

  })

  describe('push()', () => {

    it('allows a message to be added', () => {
      const err = new Pherr(testCode, 'message1')
      expect(err.message).to.be.a('string')
      expect(err.message).to.equal('message1')
      err.push('message2')
      expect(err.message).to.equal('message1\nmessage2')
    })
  })

})
