# Pherr #
Error wrapper and helpers

## Useage ##

To create a new Error instance:
`new Pherr('FAILCODE', 'Fail Reason.')`

Multiple messages can be added in the create call
`new Pherr('FAILCODE', 'Fail Reason.', 'Second Fail reason')`

Additional messages may be added after creation using `push()`
```
consr err = new Pherr('FAILCODE', 'Fail Reason.', 'Second Fail reason')
err.push('another fail')
```
However, only one error code is allowed per Error.
